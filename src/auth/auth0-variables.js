export const AUTH_CONFIG = {
  clientId: 'oL67Hs276FXrUK7V1CsOvzZgj5hfEvls',
  domain: 'dickey.auth0.com',
  callbackUrl: 'https://dashboard.dickey.cloud/callback',
  apiUrl: 'https://api.dickey.cloud'
}
